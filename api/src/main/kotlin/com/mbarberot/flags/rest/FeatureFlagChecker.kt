package com.mbarberot.flags.rest

import fr.mbarberot.feature.flags.FeatureFlagRepository
import fr.mbarberot.feature.flags.FeatureFlags
import fr.mbarberot.feature.flags.Flag
import javax.enterprise.context.ApplicationScoped
import javax.ws.rs.core.Response

@ApplicationScoped
class FeatureFlagChecker(
    val featureFlagRepository: FeatureFlagRepository
) {
    fun check(flag: Flag, block: () -> Response): Response {
        val featureFlags = FeatureFlags(featureFlagRepository)

        if (!featureFlags.isEnabled(flag)) {
            return Response
                .status(Response.Status.NOT_FOUND)
                .build()
        }

        return block.invoke()
    }
}