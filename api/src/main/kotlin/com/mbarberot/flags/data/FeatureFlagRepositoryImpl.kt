package com.mbarberot.flags.data

import fr.mbarberot.feature.flags.FeatureFlag
import fr.mbarberot.feature.flags.FeatureFlagRepository
import fr.mbarberot.feature.flags.Flag
import org.eclipse.microprofile.config.inject.ConfigProperty
import javax.enterprise.context.ApplicationScoped

@ApplicationScoped
class FeatureFlagRepositoryImpl(
    unleashClient: UnleashClient,
    @ConfigProperty(name = "feature-flags.enabled")
    val enabled: Boolean,
) : FeatureFlagRepository {

    val unleash = unleashClient.getClient()

    override fun getFeatureFlag(flag: Flag): FeatureFlag {
        return if(!enabled) {
            FeatureFlag(flag, true)
        } else {
            return FeatureFlag(flag, unleash.isEnabled(flag.key))
        }
    }

}