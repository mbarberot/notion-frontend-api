package com.mbarberot.flags.data

import io.getunleash.DefaultUnleash
import io.getunleash.Unleash
import io.getunleash.util.UnleashConfig
import org.eclipse.microprofile.config.inject.ConfigProperty
import javax.enterprise.context.ApplicationScoped

@ApplicationScoped
class UnleashClient(
    @ConfigProperty(name = "feature-flags.unleash.url")
    val apiUrl: String,
    @ConfigProperty(name = "feature-flags.unleash.instance")
    val instanceId: String,
    @ConfigProperty(name = "feature-flags.unleash.app")
    val appName: String
) {

    fun getClient(): Unleash {
        return DefaultUnleash(
            UnleashConfig.builder()
                .appName(appName)
                .instanceId(instanceId)
                .unleashAPI(apiUrl)
                .build()
        )
    }
}