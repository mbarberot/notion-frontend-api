package com.mbarberot.wishlist.data.notion

import com.mbarberot.notion.connector.types.NotionDatabaseResponse
import com.mbarberot.notion.connector.types.NotionQuery
import org.eclipse.microprofile.rest.client.annotation.RegisterClientHeaders
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient
import javax.ws.rs.POST
import javax.ws.rs.Path
import javax.ws.rs.PathParam

@RegisterRestClient(configKey = "notion-wishlist-api")
@RegisterClientHeaders(NotionWishlistHeaders::class)
interface NotionWishlistApi {

    @POST
    @Path("/databases/{database_id}/query")
    fun getItemsFromNotionDatabase(
        @PathParam("database_id") databaseId: String,
        query: NotionQuery
    ): NotionDatabaseResponse<NotionItemProperties>

}

