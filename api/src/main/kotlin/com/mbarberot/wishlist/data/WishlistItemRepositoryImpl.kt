package com.mbarberot.wishlist.data

import com.mbarberot.notion.connector.types.*
import com.mbarberot.wishlist.data.notion.NotionWishlistApi
import fr.mbarberot.wishlist.WishlistItem
import fr.mbarberot.wishlist.WishlistItemRepository
import org.eclipse.microprofile.rest.client.inject.RestClient
import javax.enterprise.context.ApplicationScoped

@ApplicationScoped
@Suppress("unused") // used through DI
class WishlistItemRepositoryImpl(
    @RestClient
    val notionWishlistApi: NotionWishlistApi
) : WishlistItemRepository {

    override fun getItems(wishlistId: String): List<WishlistItem> {
        return notionWishlistApi.getItemsFromNotionDatabase(wishlistId, createQuery())
            .results
            .map {
                WishlistItem(
                    id = it.id,
                    title = it.properties.name.title.first().plain_text,
                    description = it.properties.description.rich_text.firstOrNull()?.plain_text,
                    price = it.properties.prix.number,
                    reserved = it.properties.reserved.checkbox,
                    image = it.properties.image.files.firstOrNull()?.name,
                    link = it.properties.lien.url,
                    draft = false
                )
            }
    }
}

private fun createQuery() = NotionQuery(
    NotionFilters(or = listOf(NotionMultiSelectFilter("Tags", NotionMultiSelectDoesNotContain("Brouillon")))),
    listOf(NotionSort("Tags", NotionSortDirection.ascending))
)
