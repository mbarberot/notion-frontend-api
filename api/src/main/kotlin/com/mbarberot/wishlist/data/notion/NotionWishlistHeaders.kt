package com.mbarberot.wishlist.data.notion

import org.eclipse.microprofile.config.inject.ConfigProperty
import org.eclipse.microprofile.rest.client.ext.ClientHeadersFactory
import javax.enterprise.context.ApplicationScoped
import javax.ws.rs.core.MultivaluedHashMap
import javax.ws.rs.core.MultivaluedMap

@ApplicationScoped
class NotionWishlistHeaders(
    @ConfigProperty(name = "notion.api.version")
    val notionVersion: String,
    @ConfigProperty(name = "wishlist.notion.token")
    val notionApiToken: String
): ClientHeadersFactory {

    override fun update(
        incomingHeaders: MultivaluedMap<String, String>?,
        clientOutgoingHeaders: MultivaluedMap<String, String>?
    ): MultivaluedMap<String, String> {
        val additionnalHeaders = MultivaluedHashMap<String, String>()
        additionnalHeaders.putSingle("Authorization", notionApiToken)
        additionnalHeaders.putSingle("Notion-Version", notionVersion)
        return additionnalHeaders
    }

}