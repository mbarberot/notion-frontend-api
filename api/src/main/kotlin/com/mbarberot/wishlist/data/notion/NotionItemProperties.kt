package com.mbarberot.wishlist.data.notion

import com.fasterxml.jackson.annotation.JsonProperty
import com.mbarberot.notion.connector.types.*

data class NotionItemProperties(
    @JsonProperty("Prix")
    val prix: NotionNumberProperty,

    @JsonProperty("Lien")
    val lien: NotionUrlProperty,

    @JsonProperty("Image")
    val image: NotionFilesProperty,

    @JsonProperty("Réservé")
    val reserved: NotionCheckboxProperty,

    @JsonProperty("Description")
    val description: NotionRichTextProperty,

    @JsonProperty("Name")
    val name: NotionTextProperty
)