package com.mbarberot.wishlist.rest

import com.mbarberot.common.rest.JsonApiDataDto
import com.mbarberot.common.rest.JsonApiListResponseDto
import fr.mbarberot.wishlist.WishlistItem

object JsonApiWishlistItemMapper {
    fun toJsonApi(dtos: List<WishlistItem>) = JsonApiListResponseDto(
        dtos.map { dto -> toJsonApiAttributes(dto) }
    )

    private fun toJsonApiAttributes(item: WishlistItem) = JsonApiDataDto(
        "item",
        item.id,
        item,
    )
}