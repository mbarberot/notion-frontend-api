package com.mbarberot.wishlist.rest

import com.mbarberot.flags.rest.FeatureFlagChecker
import fr.mbarberot.feature.flags.Flag
import fr.mbarberot.wishlist.Wishlist
import fr.mbarberot.wishlist.WishlistItemRepository
import javax.ws.rs.GET
import javax.ws.rs.Path
import javax.ws.rs.PathParam
import javax.ws.rs.Produces
import javax.ws.rs.core.Response

@Path("/wishlist")
class WishlistResource(
    val wishlistItemRepository: WishlistItemRepository,
    val featureFlagChecker: FeatureFlagChecker
) {

    @GET
    @Path("/{wishlist_id}/items")
    @Produces("application/vnd.api+json")
    fun getItems(
        @PathParam("wishlist_id") wishlistId: String
    ): Response {
        return featureFlagChecker.check(Flag.Wishlist) {
            val wishlist = Wishlist(wishlistItemRepository.getItems(wishlistId))

            Response.ok(
                JsonApiWishlistItemMapper.toJsonApi(
                    wishlist.getAvailableItems()
                )
            ).build()
        }
    }
}

