package com.mbarberot.common.rest

data class JsonApiListResponseDto<T>(
    var data: List<JsonApiDataDto<T>>
)