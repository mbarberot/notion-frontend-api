package com.mbarberot.common.rest

data class JsonApiDataDto<T>(
    var type: String,
    var id: String,
    var attributes: T,
)