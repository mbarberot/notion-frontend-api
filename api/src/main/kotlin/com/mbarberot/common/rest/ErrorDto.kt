package com.mbarberot.common.rest

data class ErrorDto(
    val error: String
)
