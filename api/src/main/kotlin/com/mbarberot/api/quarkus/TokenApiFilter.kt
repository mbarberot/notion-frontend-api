package com.mbarberot.api.quarkus

import com.mbarberot.api.auth.ApiAuthenticationRequest
import com.mbarberot.api.auth.ApiAuthenticator
import com.mbarberot.api.auth.ApiUser
import com.mbarberot.api.auth.ApiRoute
import io.quarkus.logging.Log
import org.eclipse.microprofile.config.inject.ConfigProperty
import java.security.Principal
import java.time.Instant
import javax.enterprise.context.ApplicationScoped
import javax.ws.rs.container.ContainerRequestContext
import javax.ws.rs.container.ContainerRequestFilter
import javax.ws.rs.container.PreMatching
import javax.ws.rs.core.Response
import javax.ws.rs.core.SecurityContext
import javax.ws.rs.ext.Provider

@Provider
@PreMatching
@ApplicationScoped
class TokenApiFilter(
    @ConfigProperty(name = "wishlist.api.token")
    val wishlistToken: String,
    @ConfigProperty(name = "blog.api.token")
    val blogToken: String,
) : ContainerRequestFilter {

    val authenticator = ApiAuthenticator(
        listOf(
            ApiRoute(
                "/wishlist", listOf(
                    ApiUser("wishlist-app", wishlistToken)
                )
            ),
            ApiRoute(
                "/blog", listOf(
                    ApiUser("eleventy", blogToken)
                )
            )
        )
    )

    override fun filter(requestContext: ContainerRequestContext) {

        val request = ApiAuthenticationRequest(
            requestContext.uriInfo.path,
            requestContext.headers.getFirst("x_api_token")
        )

        val authentication = authenticator.authenticate(request)
        if (!authentication.authenticated) {
            abort(requestContext, "You are not allowed to use this API")
            return
        }

        requestContext.securityContext = authenticatedSecurityContext(authentication.user!!.name)
    }

    private fun authenticatedSecurityContext(username: String) = object : SecurityContext {
        override fun getUserPrincipal(): Principal = Principal { username }
        override fun isUserInRole(role: String?): Boolean = false
        override fun isSecure(): Boolean = true
        override fun getAuthenticationScheme(): String = "api-token"
    }

    private fun abort(requestContext: ContainerRequestContext, message: String) {
        Log.warn("Authentication failed. Reason : $message")
        requestContext.abortWith(
            Response.status(Response.Status.FORBIDDEN)
                .entity(Error("Authentication failed"))
                .build()
        )
    }
}

data class Error(
    val error: String,
    val timestamp: Instant = Instant.now()
)