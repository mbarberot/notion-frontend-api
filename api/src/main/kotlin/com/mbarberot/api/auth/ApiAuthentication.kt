package com.mbarberot.api.auth

sealed class ApiAuthentication(
    val authenticated: Boolean,
    val user: ApiUser?
) {
    class NotAllowed : ApiAuthentication(false,  null)
    open class Allowed(user: ApiUser) : ApiAuthentication(true, user)
    class Anonymous : Allowed(ApiUser("anonymous", ""))
}
