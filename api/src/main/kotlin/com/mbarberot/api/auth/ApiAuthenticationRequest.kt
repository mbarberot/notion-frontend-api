package com.mbarberot.api.auth

data class ApiAuthenticationRequest(
    val route: String,
    val token: String?
)