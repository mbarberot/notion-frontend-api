package com.mbarberot.api.auth

data class ApiUser(
    val name: String,
    val token: String
)