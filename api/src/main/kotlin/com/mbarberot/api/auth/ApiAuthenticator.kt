package com.mbarberot.api.auth

class ApiAuthenticator(val routes: List<ApiRoute>) {

    fun authenticate(request: ApiAuthenticationRequest): ApiAuthentication {
        val route = findRoute(request.route) ?: return ApiAuthentication.Anonymous()

        if (request.token == null) {
            return ApiAuthentication.NotAllowed()
        }

        val user = checkToken(route.users, request.token) ?: return ApiAuthentication.NotAllowed()

        return ApiAuthentication.Allowed(user)
    }

    private fun findRoute(route: String) =
        routes.find {
            route.startsWith(it.route)
        }

    private fun checkToken(users: List<ApiUser>, token: String) =
        users.find {
            it.token == token
        }
}