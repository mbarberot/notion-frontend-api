package com.mbarberot.api.auth

data class ApiRoute(
    val route: String,
    val users: List<ApiUser>
)