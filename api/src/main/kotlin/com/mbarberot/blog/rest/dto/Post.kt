package com.mbarberot.blog.rest.dto

data class Post(
    val id: String,
    val metadata: PostMetadata,
    val content: String,
    val images: List<PostImage>
)
