package com.mbarberot.blog.rest.dto

data class PostImage(
    val filename: String,
    val url: String)
