package com.mbarberot.blog.rest.dto

import java.time.LocalDate

data class PostMetadata(
    val title: String,
    val date: LocalDate,
    val tags: List<String>
)
