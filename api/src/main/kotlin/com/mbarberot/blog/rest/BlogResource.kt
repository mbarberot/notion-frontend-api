package com.mbarberot.blog.rest

import com.mbarberot.blog.rest.dto.Post
import com.mbarberot.blog.rest.dto.PostImage
import com.mbarberot.blog.rest.dto.PostMetadata
import com.mbarberot.flags.rest.FeatureFlagChecker
import fr.mbarberot.blog.Blog
import fr.mbarberot.blog.BlogPostRepository
import fr.mbarberot.feature.flags.Flag
import javax.ws.rs.GET
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType.APPLICATION_JSON
import javax.ws.rs.core.Response

@Path("/blog")
class BlogResource(
    val featureFlagChecker: FeatureFlagChecker,
    blogPostRepository: BlogPostRepository
) {

    val blog = Blog(blogPostRepository)

    @GET
    @Path("/posts")
    @Produces(APPLICATION_JSON)
    fun getPosts(
    ): Response {
        return featureFlagChecker.check(Flag.Blog) {

            val posts = blog.getPosts().map { post ->
                Post(
                    id = post.id,
                    metadata = PostMetadata(
                        title = post.title,
                        date = post.date,
                        tags = post.tags,
                    ),
                    content = post.content,
                    images = post.images.map { (name, url) -> PostImage(name, url) }
                )
            }

            return@check Response.ok(posts).build()

        }
    }
}

