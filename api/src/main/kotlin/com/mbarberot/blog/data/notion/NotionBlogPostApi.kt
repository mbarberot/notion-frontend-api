package com.mbarberot.blog.data.notion

import com.mbarberot.notion.connector.types.NotionBlock
import com.mbarberot.notion.connector.types.NotionDatabaseResponse
import com.mbarberot.notion.connector.types.NotionList
import com.mbarberot.notion.connector.types.NotionQuery
import org.eclipse.microprofile.rest.client.annotation.RegisterClientHeaders
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient
import javax.ws.rs.GET
import javax.ws.rs.POST
import javax.ws.rs.Path
import javax.ws.rs.PathParam

@RegisterRestClient(configKey = "notion-blog-api")
@RegisterClientHeaders(NotionBlogPostHeaders::class)
interface NotionBlogPostApi {

    @POST
    @Path("/databases/{database_id}/query")
    fun getPagesFromDatabase(
        @PathParam("database_id") databaseId: String,
        query: NotionQuery
    ): NotionDatabaseResponse<NotionBlogPostProperties>

    @GET
    @Path("/blocks/{page_id}/children")
    fun getBlocks(
        @PathParam("page_id") pageId: String
    ): NotionList<NotionBlock>

}


