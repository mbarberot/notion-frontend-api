package com.mbarberot.blog.data

import com.mbarberot.blog.data.notion.NotionBlogPostApi
import com.mbarberot.notion.connector.markdown.NotionToMarkdown
import com.mbarberot.notion.connector.types.*
import fr.mbarberot.blog.BlogPost
import fr.mbarberot.blog.BlogPostRepository
import org.eclipse.microprofile.config.inject.ConfigProperty
import org.eclipse.microprofile.rest.client.inject.RestClient
import javax.enterprise.context.ApplicationScoped

@ApplicationScoped
@Suppress("unused") // used through DI
class NotionBlogPostRepository(
    @RestClient
    val blogPostApi: NotionBlogPostApi,
    @ConfigProperty(name = "blog.notion.database")
    val databaseId: String,
) : BlogPostRepository {
    override fun getPosts(): List<BlogPost> {
        val notionPages = blogPostApi.getPagesFromDatabase(databaseId, createQuery())

        return notionPages.results.map { page ->

            val content = NotionToMarkdown(page.id).render(blogPostApi.getBlocks(page.id).results)

            BlogPost(
                id = page.id,
                published = page.properties.workflow.select.name == "published",
                title = page.properties.name.title.first().plain_text,
                date = page.properties.created.toLocalDate(),
                tags = page.properties.tags.multi_select.map { tag -> tag.name },
                content = content.markdown,
                images = content.images
            )
        }
    }

}


private fun createQuery() = NotionQuery(
    NotionFilters(or = listOf(NotionSelectFilter("Workflow", NotionSelectEquals("published")))),
    listOf(NotionSort("Tags", NotionSortDirection.ascending))
)
