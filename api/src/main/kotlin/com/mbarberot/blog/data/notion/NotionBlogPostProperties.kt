package com.mbarberot.blog.data.notion

import com.fasterxml.jackson.annotation.JsonProperty
import com.mbarberot.notion.connector.types.NotionDateProperty
import com.mbarberot.notion.connector.types.NotionMultiSelectProperty
import com.mbarberot.notion.connector.types.NotionSelectProperty
import com.mbarberot.notion.connector.types.NotionTextProperty


data class NotionBlogPostProperties(
    @JsonProperty("Tags")
    val tags: NotionMultiSelectProperty,

    @JsonProperty("Created")
    val created: NotionDateProperty,

    @JsonProperty("Workflow")
    val workflow: NotionSelectProperty,

    @JsonProperty("Name")
    val name: NotionTextProperty,
)


