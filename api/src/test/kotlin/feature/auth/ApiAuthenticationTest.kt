package feature.auth

import com.mbarberot.api.auth.ApiAuthenticationRequest
import com.mbarberot.api.auth.ApiAuthenticator
import com.mbarberot.api.auth.ApiUser
import com.mbarberot.api.auth.ApiRoute
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class ApiAuthenticationTest {

    @Test
    internal fun `authentication is Anonymous by default`() {
        // arrange

        // act
        val response = ApiAuthenticator(listOf()).authenticate(
            ApiAuthenticationRequest("/api/wishlist", "azerty")
        )

        // assert
        assertThat(response.authenticated).isTrue
        assertThat(response.user).isEqualTo(ApiUser("anonymous", ""))
    }

    @Test
    internal fun `authenticated route requires a correct token`() {
        // arrange
        val authenticatedApiRoutes = listOf(
            ApiRoute("/api/wishlist", listOf(ApiUser("wishlist-app", "azerty")))
        )

        // act
        val response = ApiAuthenticator(authenticatedApiRoutes).authenticate(
            ApiAuthenticationRequest("/api/wishlist", "azerty")
        )

        // assert
        assertThat(response.authenticated).isTrue
        assertThat(response.user).isEqualTo(ApiUser("wishlist-app", "azerty"))
    }

    @Test
    internal fun `every subroute is authenticated`() {
        // arrange
        val authenticatedApiRoutes = listOf(
            ApiRoute("/api/wishlist", listOf(ApiUser("wishlist-app", "azerty")))
        )

        // act
        val response = ApiAuthenticator(authenticatedApiRoutes).authenticate(
            ApiAuthenticationRequest("/api/wishlist/items", "azerty")
        )

        // assert
        assertThat(response.authenticated).isTrue
        assertThat(response.user).isEqualTo(ApiUser("wishlist-app", "azerty"))
    }

    @Test
    internal fun `a wrong token fail to authenticate`() {
        // arrange
        val apiRoutes = listOf(
            ApiRoute("/api/wishlist", listOf(ApiUser("wishlist-app", "azerty")))
        )

        // act
        val response = ApiAuthenticator(apiRoutes).authenticate(
            ApiAuthenticationRequest("/api/wishlist", "wrongtoken")
        )

        // assert
        assertThat(response.authenticated).isFalse
        assertThat(response.user).isNull()
    }

    @Test
    internal fun `no token fail to authenticate`() {
        // arrange
        val apiRoutes = listOf(
            ApiRoute("/api/wishlist", listOf(ApiUser("wishlist-app", "azerty")))
        )

        // act
        val response = ApiAuthenticator(apiRoutes).authenticate(
            ApiAuthenticationRequest("/api/wishlist", null)
        )

        // assert
        assertThat(response.authenticated).isFalse
        assertThat(response.user).isNull()
    }

}

