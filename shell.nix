with (import <nixpkgs> {});

mkShell {
    buildInputs = [
        jdk11
        (gradle.override { java = jdk11; })
    ];

    shellHook = ''
      export JAVA_HOME=${jdk11.home}
    '';
}
