package feature.flags

import fr.mbarberot.feature.flags.FeatureFlag
import fr.mbarberot.feature.flags.FeatureFlagRepository
import fr.mbarberot.feature.flags.FeatureFlags
import fr.mbarberot.feature.flags.Flag
import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe
import io.mockk.every
import io.mockk.mockk

fun repositoryWithFlag(wishlist: FeatureFlag? = null, blog: FeatureFlag? = null): FeatureFlagRepository {
    val featureFlagRepository = mockk<FeatureFlagRepository>()
    every { featureFlagRepository.getFeatureFlag(Flag.Wishlist) } returns wishlist
    every { featureFlagRepository.getFeatureFlag(Flag.Blog) } returns blog
    return featureFlagRepository
}

class IsEnabledTest : StringSpec({
    "return true is the feature is enabled" {
        // arrange
        val featureFlags = FeatureFlags(
            repositoryWithFlag(FeatureFlag(Flag.Wishlist, true))
        )

        // act
        val isEnabled = featureFlags.isEnabled(Flag.Wishlist)

        // assert
        isEnabled shouldBe true
    }

    "return false when the feature is disabled" {
        // arrange
        val featureFlags = FeatureFlags(
            repositoryWithFlag(FeatureFlag(Flag.Wishlist, false))
        )

        // act
        val isEnabled = featureFlags.isEnabled(Flag.Wishlist)

        // assert
        isEnabled shouldBe false
    }

    "return false when the feature is not registered" {
        // arrange
        val featureFlags = FeatureFlags(
            repositoryWithFlag(FeatureFlag(Flag.Wishlist, false))
        )

        // act
        val isEnabled = featureFlags.isEnabled(Flag.Blog)

        // assert
        isEnabled shouldBe false
    }

})