import fr.mbarberot.blog.BlogPost
import fr.mbarberot.wishlist.WishlistItem

class Faker : net.datafaker.Faker() {
    fun wishlist() = getProvider(WishlistFaker::class.java) { WishlistFaker(this) }
    fun blog() = getProvider(BlogFaker::class.java) { BlogFaker(this) }
}

class BlogFaker(val faker: Faker) {
    fun post(title: String, published: Boolean = true) = BlogPost(
        id = faker.idNumber().valid(),
        title = title,
        content = """
        # ${faker.lorem().sentences(1)}
        
        ${faker.lorem().sentences(5)}
        """.trimIndent(),
        tags = faker.lorem().words(2),
        images = mapOf(),
        published = published
    )
}
class WishlistFaker(val faker: Faker) {

    fun item(
        draft: Boolean?
    ) = WishlistItem(
        id = faker.idNumber().valid(),
        title = faker.lorem().sentence(3),
        price = faker.number().numberBetween(10, 200),
        reserved = faker.bool().bool(),
        image = faker.internet().image(),
        link = faker.internet().url(),
        description = faker.lorem().sentence(),
        draft = draft ?: faker.bool().bool()
    )

}