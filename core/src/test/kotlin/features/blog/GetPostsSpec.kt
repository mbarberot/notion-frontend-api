package features.blog

import Faker
import fr.mbarberot.blog.Blog
import fr.mbarberot.blog.BlogPost
import fr.mbarberot.blog.BlogPostRepository
import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.collections.shouldContainExactly
import io.kotest.matchers.collections.shouldHaveSize
import io.mockk.every
import io.mockk.mockk

@Suppress("unused") // Used by testing framework
class GetPostsSpec : StringSpec({

    val faker = Faker()

    "a blog have posts" {
        // Arrange
        val postRepository = repositoryWithPosts(
            listOf(
                faker.blog().post("Learn Kotlin in five steps")
            )
        )
        val blog = Blog(postRepository)

        // Act
        val posts = blog.getPosts()

        // Assert
        posts shouldHaveSize 1
        posts.map { it.title } shouldContainExactly listOf("Learn Kotlin in five steps")
    }

    "a blog only shows published posts" {
        // Arrange
        val repository = repositoryWithPosts(
            listOf(
                faker.blog().post("Learn Kotlin in five steps", published = true),
                faker.blog().post("Learn Java by Example", published = false)
            )
        )
        val blog = Blog(repository)

        // Act
        val posts = blog.getPosts()

        // Assert
        posts shouldHaveSize 1
        posts.map { it.title } shouldContainExactly listOf("Learn Kotlin in five steps")
    }
})

private fun repositoryWithPosts(posts: List<BlogPost> = listOf()): BlogPostRepository {
    val postRepository = mockk<BlogPostRepository>()
    every { postRepository.getPosts() } returns posts
    return postRepository
}

