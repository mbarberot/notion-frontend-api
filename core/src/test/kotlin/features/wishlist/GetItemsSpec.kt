package features.wishlist

import Faker
import fr.mbarberot.wishlist.Wishlist
import fr.mbarberot.wishlist.WishlistItem
import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe

@Suppress("unused")
class GetItemsSpec : StringSpec({

    val faker = Faker()

    "should not return any items when wishlist is empty" {
        // given
        val items = listOf<WishlistItem>()

        // when
        val availableItems = Wishlist(items).getAvailableItems()

        // then
        availableItems.size shouldBe 0
    }

    "should only return validated items" {
        // given
        val items = listOf(
            faker.wishlist().item(draft = false),
            faker.wishlist().item(draft = true),
        )

        // when
        val availableItems = Wishlist(items).getAvailableItems()

        // then
        availableItems.size shouldBe 1
    }
})