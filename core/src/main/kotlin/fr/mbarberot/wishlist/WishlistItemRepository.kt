package fr.mbarberot.wishlist

interface WishlistItemRepository {
    fun getItems(wishlistId: String): List<WishlistItem>
}