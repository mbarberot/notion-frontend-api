package fr.mbarberot.wishlist

class Wishlist(val items: List<WishlistItem>) {
    fun getAvailableItems(): List<WishlistItem> {
        return items.filter { !it.draft }
    }

}
