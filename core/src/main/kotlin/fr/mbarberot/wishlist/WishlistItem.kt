package fr.mbarberot.wishlist

data class WishlistItem(
    val id: String,
    val title: String,
    val price: Int,
    val reserved: Boolean,
    val image: String?,
    val link: String?,
    val description: String?,
    val draft: Boolean = true
)

