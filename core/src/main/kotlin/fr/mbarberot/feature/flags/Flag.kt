package fr.mbarberot.feature.flags

enum class Flag(
    val key: String
) {
    Wishlist("wishlist-api"),
    Blog("blog-api")
}