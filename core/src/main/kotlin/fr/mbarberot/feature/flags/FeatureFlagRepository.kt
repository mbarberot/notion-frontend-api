package fr.mbarberot.feature.flags

interface FeatureFlagRepository {
    fun getFeatureFlag(flag: Flag): FeatureFlag?
}
