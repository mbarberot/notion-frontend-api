package fr.mbarberot.feature.flags

class FeatureFlag(
    val name: Flag,
    val enabled: Boolean
)
