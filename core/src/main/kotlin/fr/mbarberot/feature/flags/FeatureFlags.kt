package fr.mbarberot.feature.flags

class FeatureFlags(val repository: FeatureFlagRepository) {
    fun isEnabled(flag: Flag): Boolean {
        return repository.getFeatureFlag(flag)?.enabled ?: false
    }
}
