package fr.mbarberot.blog

class Blog(
    private val blogPostRepository: BlogPostRepository
) {
    fun getPosts(): List<BlogPost> = blogPostRepository.getPosts().filter { it.published }
}