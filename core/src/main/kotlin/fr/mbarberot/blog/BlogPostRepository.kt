package fr.mbarberot.blog

interface BlogPostRepository {
    fun getPosts(): List<BlogPost>
}