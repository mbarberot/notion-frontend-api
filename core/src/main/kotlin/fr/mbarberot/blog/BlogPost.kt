package fr.mbarberot.blog

import java.time.LocalDate

data class BlogPost(
    val id: String,
    val published: Boolean = false,
    val title: String,
    val content: String,
    val date: LocalDate = LocalDate.now(),
    val tags: List<String> = listOf(),
    val images: Map<String,String> = mapOf(),
)