plugins {
    kotlin("jvm")
    id("com.adarshr.test-logger") version "3.2.0"
    id("org.kordamp.gradle.jandex") version "1.1.0"
    jacoco
}

group = "fr.mbarberot"
version = "1.0.0-SNAPSHOT"

dependencies {
    implementation(kotlin("stdlib"))

    val kotestVersion = "5.2.3"
    val mockkVersion = "1.12.3"
    testImplementation(kotlin("test"))
    testImplementation("io.kotest:kotest-runner-junit5:${kotestVersion}")
    testImplementation("io.kotest:kotest-assertions-core:${kotestVersion}")
    testImplementation("io.mockk:mockk:${mockkVersion}")
    testImplementation("net.datafaker:datafaker:1.3.0")
}

tasks {
    withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
        kotlinOptions.jvmTarget = "11"
    }

    test {
        finalizedBy(jacocoTestReport)
        useJUnitPlatform()
    }

    jacocoTestReport {
        dependsOn(test)
        reports {
            xml.required.set(true)
            xml.outputLocation.set(layout.buildDirectory.file("jacoco/report.xml"))
            html.required.set(true)
            html.outputLocation.set(layout.buildDirectory.dir("jacoco/html"))
        }
    }
}

testlogger {
    setTheme("mocha")
}

jacoco {
    toolVersion = "0.8.8"
}

