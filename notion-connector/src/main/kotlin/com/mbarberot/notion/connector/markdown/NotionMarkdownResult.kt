package com.mbarberot.notion.connector.markdown

data class NotionMarkdownResult(
    val markdown: String,
    val images: Map<String, String>
)