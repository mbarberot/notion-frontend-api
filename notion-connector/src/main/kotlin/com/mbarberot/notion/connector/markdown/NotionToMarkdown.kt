package com.mbarberot.notion.connector.markdown

import com.mbarberot.notion.connector.types.*

class NotionToMarkdown(
    private val pageId: String,
    private val defaultCodeLanguage: String = "bash",
) {

    private val builder = StringBuilder()
    private val images: MutableMap<String, String> = mutableMapOf()

    fun render(blocks: List<NotionBlock>): NotionMarkdownResult {

        blocks.forEachIndexed { index, block ->
            when (block.type) {
                "heading_1" -> renderHeading(1, block.heading_1)
                "heading_2" -> renderHeading(2, block.heading_2)
                "heading_3" -> renderHeading(3, block.heading_3)
                "code" -> renderCode(block.code)
                "paragraph" -> renderParagraph(block.paragraph)
                "quote" -> renderQuote(block.quote)
                "bulleted_list_item" -> renderList(block.bulleted_list_item, isLastBulletPoint(blocks, index))
                "image" -> renderImage(block.image)
                "callout" -> renderParagraph(block.callout)
            }
        }

        return NotionMarkdownResult(
            markdown = builder.toString(),
            images = images,
        )
    }

    private fun renderImage(image: NotionImage?) {
        if (image == null) {
            return
        }

        val imageCounter = images.size + 1
        val imageName = "image-$imageCounter"
        val imageText = image.caption.firstOrNull()?.plain_text ?: imageName
        val imageExtension = extractExtensionFromURL(image.file.url)
        val fileName = "$imageName.$imageExtension"

        builder.appendLine("![$imageText](/notion/$pageId/$fileName)")
        builder.appendLine()

        images[fileName] = image.file.url
    }


    private fun renderList(listItem: NotionBulletedListItem?, isLastBulletPoint: Boolean) {
        if (listItem == null) {
            return
        }

        builder.appendLine("* ${listItem.rich_text.first().plain_text}")

        if (isLastBulletPoint) {
            builder.appendLine()
        }
    }

    private fun renderQuote(quote: NotionQuote?) {
        if (quote == null) {
            return
        }

        quote.rich_text
            .first()
            .plain_text
            .split("\n")
            .map { "> ${it}" }
            .forEach { builder.appendLine(it) }

        builder.appendLine()
    }

    private fun renderHeading(level: Int, heading: NotionHeading?) {
        if (heading == null) {
            return
        }

        val titleLevel = (1..level).joinToString("") { "#" }

        builder.appendLine("$titleLevel ${heading.rich_text.first().plain_text}")
        builder.appendLine()
    }

    private fun renderCode(code: NotionCode?) {
        if (code == null) {
            return
        }

        val language = when (code.language) {
            "plain text" -> defaultCodeLanguage
            else -> code.language
        }

        code.rich_text.forEach {
            builder
                .appendLine("```$language")
                .append(it.plain_text)
                .appendLine()
                .appendLine("```")
        }
        builder.appendLine()
    }

    private fun renderParagraph(paragraph: NotionParagraph?) {
        if (paragraph == null) {
            return
        }

        paragraph.rich_text.forEach {
            when {
                it.annotations.code -> builder.append("`${it.plain_text}`")
                it.href != null -> builder.append("[${it.plain_text}](${it.href})")
                else -> builder.append(it.plain_text)
            }
        }

        builder.appendLine()
        builder.appendLine()
    }
}

private fun isLastBulletPoint(blocks: List<NotionBlock>, index: Int): Boolean {
    val isLastBlock = index + 1 >= blocks.size
    val nextBlockIsBulletPoint = !isLastBlock && blocks[index + 1].type == "bulleted_list_item"
    return when {
        isLastBlock -> true
        nextBlockIsBulletPoint -> false
        else -> true
    }
}

private fun extractExtensionFromURL(url: String) = url
    .split("/").last()
    .split("?").first()
    .split(".").last()