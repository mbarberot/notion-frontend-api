package com.mbarberot.notion.connector.types


data class NotionList<T>(
    val results: List<T>
)

data class NotionParagraph(
    val rich_text: List<NotionRichText>
)

data class NotionCode(
    val rich_text: List<NotionRichText>,
    val language: String
)

data class NotionHeading(
    val rich_text: List<NotionRichText>,
)

data class NotionQuote(
    val rich_text: List<NotionRichText>,
)
data class NotionBulletedListItem(
    val rich_text: List<NotionRichText>,
)

data class NotionImage(
    val file: NotionFile,
    val caption: List<NotionRichText> = listOf()
)

data class NotionFile(
    val url: String
)

data class NotionBlock(
    val id: String,
    val type: String,
    val paragraph: NotionParagraph? = null,
    val code: NotionCode? = null,
    val heading_1: NotionHeading? = null,
    val heading_2: NotionHeading? = null,
    val heading_3: NotionHeading? = null,
    val quote: NotionQuote? = null,
    val bulleted_list_item: NotionBulletedListItem? = null,
    val image: NotionImage? = null,
    val callout: NotionParagraph? = null,
)
