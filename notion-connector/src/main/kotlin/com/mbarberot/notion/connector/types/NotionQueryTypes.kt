package com.mbarberot.notion.connector.types


data class NotionQuery(
    val filter: NotionFilters,
    val sorts: List<NotionSort>
)

data class NotionFilters(
    val or: List<NotionFilter>
)

interface NotionFilter {
    val property: String
}

data class NotionMultiSelectFilter(
    override val property: String,
    val multi_select: NotionMultiSelectDoesNotContain
): NotionFilter

data class NotionMultiSelectDoesNotContain(
    val does_not_contain: String
)

data class NotionSelectFilter(
    override val property: String,
    val select: NotionSelectEquals
): NotionFilter

data class NotionSelectEquals(
    val equals: String
)

data class NotionSort(
    val property: String,
    val direction: NotionSortDirection
)

enum class NotionSortDirection {
    ascending
}