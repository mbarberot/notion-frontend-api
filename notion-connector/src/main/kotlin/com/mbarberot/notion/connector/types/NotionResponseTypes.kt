package com.mbarberot.notion.connector.types

import java.time.ZonedDateTime


data class NotionDatabaseResponse<T>(
    val results: List<NotionPage<T>>
)

data class NotionPage<T>(
    val id: String,
    val properties: T
)

data class NotionNumberProperty(
    val number: Int
)

data class NotionUrlProperty(
    val url: String?
)

data class NotionFilesProperty(
    val files: List<NotionFileProperty>
)

data class NotionFileProperty(
    val name: String
)

data class NotionCheckboxProperty(
    val checkbox: Boolean
)

data class NotionRichTextProperty(
    val rich_text: List<NotionRichText>
)

data class NotionRichText(
    val plain_text: String,
    val annotations: NotionRichTextAnnotations = NotionRichTextAnnotations(),
    val href: String? = null
)

data class NotionRichTextAnnotations(
    val bold: Boolean = false,
    val italic: Boolean = false,
    val strikethrough: Boolean = false,
    val underline: Boolean = false,
    val code: Boolean = false,
    val color: String = "default",
)

data class NotionTextProperty(
    val title: List<NotionTitle>
)

data class NotionTitle(
    val plain_text: String
)

data class NotionMultiSelectProperty(
    val multi_select: List<SelectItem>
)

data class NotionSelectProperty(
    val select: SelectItem
)

data class SelectItem(
    val name: String
)

data class NotionDateProperty(
    val created_time: String
) {
    fun toDate() = ZonedDateTime.parse(created_time) // should be ISO String
    fun toLocalDate() = toDate().toLocalDate()
}