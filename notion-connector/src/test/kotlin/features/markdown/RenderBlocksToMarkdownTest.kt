package features.markdown

import com.mbarberot.notion.connector.markdown.NotionToMarkdown
import com.mbarberot.notion.connector.types.NotionBlock
import com.mbarberot.notion.connector.types.NotionRichText
import com.mbarberot.notion.connector.types.NotionRichTextAnnotations
import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.maps.shouldContain
import io.kotest.matchers.shouldBe


@Suppress("unused") // Used by test framework
class RenderBlocksToMarkdownTest : StringSpec({

    val blockFactory = BlockFactory()

    "render all" {
        // arrange
        val blocks = listOf(
            blockFactory.heading(1, "Quibusdam incidunt est suscipit quasi."),
            blockFactory.paragraphBlock(
                """
                    Dolor molestiae voluptas ipsum earum esse. Maxime assumenda eveniet quisquam et natus voluptas. 
                    Laboriosam illo fuga est adipisci. A inventore hic dolor aut rerum sit aut. Ducimus nobis non accusamus aperiam iste et in temporibus.
                """.trimIndent()
            ),
            blockFactory.listBlock("Incidunt sint unde nobis ut minima."),
            blockFactory.listBlock("Ea consectetur tenetur error est rerum nostrum officiis."),
            blockFactory.quote(
                """
                    Unde tenetur quidem qui ea. Earum molestiae nam commodi autem fugit qui. 
                    Ut non porro enim possimus est saepe. Error quos blanditiis ab et ut ea laudantium. 
                    Eaque voluptas nulla repellendus dolores rerum. 
                """.trimIndent()
            ),
            blockFactory.heading(2, "Incidunt sint unde nobis ut minima"),
            blockFactory.paragraphBlock(
                NotionRichText(
                    """
                        Adipisci aperiam possimus officiis eveniet accusamus aperiam dicta. 
                        Repellendus 
                    """.trimIndent()
                ),
                NotionRichText("laboriosam doloremque", NotionRichTextAnnotations(code = true)),
                NotionRichText(" eius molestiae rerum. Voluptatem molestiae enim totam."),
            ),
            blockFactory.codeBlock(
                "java",
                """
                    public class Foo {
                        private final String bar;
                    }
                """.trimIndent()
            ),
            blockFactory.listBlock("Eos quis deleniti et velit tenetur quia minus."),
            blockFactory.listBlock("Et quia qui ipsam veritatis."),
        )

        // act
        val result = renderToMarkdown(blocks)

        // assert
        result.markdown shouldBe """
            # Quibusdam incidunt est suscipit quasi.
            
            Dolor molestiae voluptas ipsum earum esse. Maxime assumenda eveniet quisquam et natus voluptas. 
            Laboriosam illo fuga est adipisci. A inventore hic dolor aut rerum sit aut. Ducimus nobis non accusamus aperiam iste et in temporibus.
            
            * Incidunt sint unde nobis ut minima.
            * Ea consectetur tenetur error est rerum nostrum officiis.
            
            > Unde tenetur quidem qui ea. Earum molestiae nam commodi autem fugit qui. 
            > Ut non porro enim possimus est saepe. Error quos blanditiis ab et ut ea laudantium. 
            > Eaque voluptas nulla repellendus dolores rerum. 
            
            ## Incidunt sint unde nobis ut minima
            
            Adipisci aperiam possimus officiis eveniet accusamus aperiam dicta. 
            Repellendus `laboriosam doloremque` eius molestiae rerum. Voluptatem molestiae enim totam.
            
            ```java
            public class Foo {
                private final String bar;
            }
            ```
            
            * Eos quis deleniti et velit tenetur quia minus.
            * Et quia qui ipsam veritatis.
            
            
        """.trimIndent()
    }

    "render simple paragraph" {
        // arrange
        val block = blockFactory.paragraphBlock(
            """
            Dolor molestiae voluptas ipsum earum esse. Maxime assumenda eveniet quisquam et natus voluptas. 
            Laboriosam illo fuga est adipisci. A inventore hic dolor aut rerum sit aut. Ducimus nobis non accusamus aperiam iste et in temporibus.
        """.trimIndent()
        )

        // act
        val result = renderToMarkdown(block)

        // assert
        result.markdown shouldBe """
            Dolor molestiae voluptas ipsum earum esse. Maxime assumenda eveniet quisquam et natus voluptas. 
            Laboriosam illo fuga est adipisci. A inventore hic dolor aut rerum sit aut. Ducimus nobis non accusamus aperiam iste et in temporibus.
            
            
        """.trimIndent()
    }

    "render paragraph with inline code block" {
        // arrange
        val block = blockFactory.paragraphBlock(
            NotionRichText(
                """
                Adipisci aperiam possimus officiis eveniet accusamus aperiam dicta. 
                Repellendus 
            """.trimIndent()
            ),
            NotionRichText("laboriosam doloremque", NotionRichTextAnnotations(code = true)),
            NotionRichText(" eius molestiae rerum. Voluptatem molestiae enim totam."),
        )

        // act
        val result = renderToMarkdown(block)

        // assert
        result.markdown shouldBe """
            Adipisci aperiam possimus officiis eveniet accusamus aperiam dicta. 
            Repellendus `laboriosam doloremque` eius molestiae rerum. Voluptatem molestiae enim totam.
            
            
        """.trimIndent()
    }

    "render paragraph with link" {
        // arrange
        val block = blockFactory.paragraphBlock(
            NotionRichText(plain_text = "Quidem qui quo "),
            NotionRichText(plain_text = "voluptas ullam", href = "http://laudantium.com"),
            NotionRichText(plain_text = " aut.")
        )

        // act
        val result = renderToMarkdown(block)

        // assert
        result.markdown shouldBe """
            Quidem qui quo [voluptas ullam](http://laudantium.com) aut.
            
            
        """.trimIndent()
    }

    "render headings" {
        // arrange
        val blocks = listOf(
            blockFactory.heading(1, "Quibusdam incidunt est suscipit quasi."),
            blockFactory.heading(2, "Incidunt sint unde nobis ut minima"),
            blockFactory.heading(3, "Et quia qui ipsam veritatis"),
        )

        // act
        val result = renderToMarkdown(blocks)

        // assert
        result.markdown shouldBe """
            # Quibusdam incidunt est suscipit quasi.
            
            ## Incidunt sint unde nobis ut minima
            
            ### Et quia qui ipsam veritatis
            
            
        """.trimIndent()
    }

    "render quote" {
        // arrange
        val block = blockFactory.quote(
            """
            Unde tenetur quidem qui ea. Earum molestiae nam commodi autem fugit qui. 
            Ut non porro enim possimus est saepe. Error quos blanditiis ab et ut ea laudantium. 
            Eaque voluptas nulla repellendus dolores rerum. 
        """.trimIndent()
        )

        // act
        val result = renderToMarkdown(block)

        // assert
        result.markdown shouldBe """
            > Unde tenetur quidem qui ea. Earum molestiae nam commodi autem fugit qui. 
            > Ut non porro enim possimus est saepe. Error quos blanditiis ab et ut ea laudantium. 
            > Eaque voluptas nulla repellendus dolores rerum. 
            
            
        """.trimIndent()
    }

    "render list" {
        // arrange
        val blocks = listOf(
            blockFactory.listBlock("Incidunt sint unde nobis ut minima."),
            blockFactory.listBlock("Ea consectetur tenetur error est rerum nostrum officiis."),
            blockFactory.listBlock("Eos quis deleniti et velit tenetur quia minus."),
            blockFactory.listBlock("Et quia qui ipsam veritatis."),
        )

        // act
        val result = renderToMarkdown(blocks)

        // assert
        result.markdown shouldBe """
            * Incidunt sint unde nobis ut minima.
            * Ea consectetur tenetur error est rerum nostrum officiis.
            * Eos quis deleniti et velit tenetur quia minus.
            * Et quia qui ipsam veritatis.
            
            
        """.trimIndent()
    }

    "render code" {
        // arrange
        val block = blockFactory.codeBlock(
            "plain text",
            """
                public class Foo {
                    private final String bar;
                }
            """.trimIndent()
        )

        // act
        val result = renderToMarkdown(block)

        // assert
        result.markdown shouldBe """
            ```bash
            public class Foo {
                private final String bar;
            }
            ```
            
            
        """.trimIndent()
    }

    "render code with language" {
        // arrange
        val block = blockFactory.codeBlock(
            "java",
            """
                public class Foo {
                    private final String bar;
                }
            """.trimIndent()
        )

        // act
        val result = renderToMarkdown(block)

        // assert
        result.markdown shouldBe """
            ```java
            public class Foo {
                private final String bar;
            }
            ```
            
            
        """.trimIndent()
    }

    "render callout" {
        // arrange
        val block = blockFactory.calloutBlock(
            """
                Vel natus ratione qui tempore alias voluptas molestiae occaecati. Nesciunt ipsum voluptatem quasi optio architecto repellendus et. 
                Vel voluptatem eligendi amet sint ut nostrum. Quia eligendi ut autem et.
            """.trimIndent()
        )

        // act
        val result = renderToMarkdown(block)

        // assert
        result.markdown shouldBe """
             Vel natus ratione qui tempore alias voluptas molestiae occaecati. Nesciunt ipsum voluptatem quasi optio architecto repellendus et. 
             Vel voluptatem eligendi amet sint ut nostrum. Quia eligendi ut autem et.
            
            
        """.trimIndent()
    }

    "render images" {
        // arrange
        pageId = "8324-1509-fb12"
        val block = blockFactory.imageBlock(
            "https://laboriosam.com/quos/et/autem/et/illum/Inventore.png?quis=et&qui=voluptatem",
            "Qui omnis perferendis odit",
        )

        // act
        val result = renderToMarkdown(block)

        // assert
        result.markdown shouldBe """
            ![Qui omnis perferendis odit](/notion/8324-1509-fb12/image-1.png)
            
            
        """.trimIndent()

        result.images shouldContain Pair(
            "image-1.png",
            "https://laboriosam.com/quos/et/autem/et/illum/Inventore.png?quis=et&qui=voluptatem"
        )
    }


})

var pageId: String = "foo"

fun renderToMarkdown(block: NotionBlock) = renderToMarkdown(listOf(block))

fun renderToMarkdown(blocks: List<NotionBlock>) = NotionToMarkdown(pageId).render(blocks)