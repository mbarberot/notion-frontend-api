package features.markdown

import com.mbarberot.notion.connector.types.*

class BlockFactory(
    private val id: String = "foo"
) {

    fun paragraphBlock(vararg texts: String) =
        NotionBlock(
            id,
            "paragraph",
            paragraph = NotionParagraph(texts.map { NotionRichText(it) })
        )

    fun paragraphBlock(vararg texts: NotionRichText) =
        NotionBlock(
            id,
            "paragraph",
            paragraph = NotionParagraph(texts.asList())
        )

    fun codeBlock(language: String, code: String) =
        NotionBlock(
            id,
            "code",
            code = NotionCode(
                listOf(NotionRichText(code)),
                language
            )
        )

    fun heading(level: Int, text: String): NotionBlock {

        val heading = NotionHeading(
            listOf(NotionRichText(text))
        )

        return when (level) {
            1 -> NotionBlock(id, "heading_1", heading_1 = heading)
            2 -> NotionBlock(id, "heading_2", heading_2 = heading)
            3 -> NotionBlock(id, "heading_3", heading_3 = heading)
            else -> throw Error("Level $level is not supported")
        }
    }

    fun quote(text: String) =
        NotionBlock(
            id,
            "quote",
            quote = NotionQuote(
                listOf(NotionRichText(text))
            )
        )

    fun listBlock(text: String) =
        NotionBlock(
            id,
            "bulleted_list_item",
            bulleted_list_item = NotionBulletedListItem(
                listOf(
                    NotionRichText(text)
                )
            )
        )

    fun imageBlock(url: String, caption: String) =
        NotionBlock(
            id,
            "image",
            image = NotionImage(
                NotionFile(url),
                listOf(NotionRichText(caption))
            )
        )

    fun calloutBlock(text: String) =
        NotionBlock(
            id,
            "callout",
            callout = NotionParagraph(
                listOf(NotionRichText(text))
            )
        )
}